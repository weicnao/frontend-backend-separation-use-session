const express = require('express');
const session = require('express-session')
const app = express();
var sess = {
  secret: 'keyboard cat',
  cookie: {
    httpOnly: true,
    sameSite:true
  },
  resave: true,
  saveUninitialized: true,
}
app.use(session(sess))

app.post('/api/test', (req, res) => {
    console.log(req.sessionID);
    res.json({ID: req.sessionID})
})

app.listen(7777, () => {
    console.log('start');
});