## Requirements

* NodeJS
* Git
* Nginx

## Common setup

Clone the repo.

```bash
git clone git@gitlab.com:weicnao/frontend-backend-separation-use-session.git
```

## WEB

Install the dependencies.

```bash
cd web
npm install
```

Run web server.

```bash
node web.js
```

## AP

Install the dependencies.

```bash
cd ap
npm install
```
Run ap server.

```bash
node index.js
```

## Nginx

Please copy the nginx.conf to your nginx path, you can use fllow command to know the path and reload it.
## The Nginx Command
The `nginx` command can be used to perform some useful actions when Nginx is running.

- Get current Nginx version and its configured compiling parameters: `nginx -V`
- Test the current Nginx configuration file and / or check its location: `nginx -t`
- Reload the configuration without restarting Nginx: `nginx -s reload`


## Vist The Page

http://localhost  `same site`

http://localhost:8080 `cross site`
